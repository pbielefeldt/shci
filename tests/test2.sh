#!/usr/bin/env bash

if cat ${CI_PROJECT_DIR}/text.txt | grep -q "consetetur sadipscing elitr"; then
    echo "nice, test file contains \"consetetur sadipscing elitr\"";
else
    echo "oops, test file doesn't contain searched string";
    cat ${CI_PROJECT_DIR}/text.txt;
    return 51;
fi

return 0;
