#!/usr/bin/env bash

if cat ${CI_PROJECT_DIR}/text.txt | grep -q "lorem ipsum"; then
    echo "nice, test file contains \"lorem ipsum\"";
else
    echo "oops, test file doesn't contain searched string";
    return 51;
fi

return 0;
